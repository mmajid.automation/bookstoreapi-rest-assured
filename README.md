# Test API
This project was created to start the initial steps with test automation for a REST API using RestAssured.

> :warning: **Disclaimer**
>
> This project does not have the best practices that could be applied
> demonstrate an example of running tests for api in a pipeline
> but the central point of this repository and demonstrate an example of running tests for api in a pipeline

## Required softwares
* Java JDK 11+
* Maven installed

## How to execute the tests
You can open each test classes on `src\test\java` and execute all of them, but I recommend you run it by the
command line. It enables us to run in different test execution strategies and also in a pipeline, that is the propose of this repo.

## About the environments
You can run the tests simulation two environments: dev and test.
By default, the scripts use the test environment.

## About the Project Structure

### conf
Configuration simulation different environments:
* dev: development environment
* test: test/pre-prod environment

### schemas
JSON Schemas to enable Contract Testing

### suites
TestNG suites divided by a pipeline strategy


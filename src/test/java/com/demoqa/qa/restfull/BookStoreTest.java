package com.demoqa.qa.restfull;

import com.demoqa.qa.base.BaseAPI;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalToIgnoringCase;
/**
 * @author mohammad.majid
 */
public class BookStoreTest extends BaseAPI {


    @Test(groups = "functional")
    public void verifyBooks(){
        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.contentType(ContentType.JSON);
        requestSpecification.accept(ContentType.JSON);
        Response response = requestSpecification.get("/Books");
        int statusCode =  response.statusCode();
        Assert.assertEquals(statusCode,200);
        Assert.assertEquals(response.contentType(),"application/json; charset=utf-8");
        //String body = response.asString();
        //System.out.println(body);
        String jsonString = response.asString();
        System.out.println(jsonString);
        //String userId = with(jsonString).get("subTitle").toString();
        //System.out.println(userId);
        //this.createdUserId = userId;
    }

    @Test(groups = "e2e")
    public void verifyAllBooks(){
        given()
                .log().all()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
        .when()
                .get("/Books")
        .then()
                .log().body()
                .statusCode(200)
                .body(matchesJsonSchemaInClasspath("schemas/books.json"));
    }

    @Test(groups = "smoke")
    public void verifySpecificBooks(){
        given()
                .log().all()
                .param("ISBN", "9781593275846")
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
        .when()
                .get("/Book")
        .then()
                .log().body()
                .statusCode(200)
                .body(matchesJsonSchemaInClasspath("schemas/books.json"));
    }

    @Test(groups = "regression")
    public void verifyForInvalidISBNI(){
        given()
                .log().all()
                .param("ISBN", "0")
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .get("/Book")
                .then()
                .log().body()
                .statusCode(400)
                .body("code", equalToIgnoringCase("1205"))
                .body("message", equalToIgnoringCase("ISBN supplied is not available in Books Collection!"));
    }

}
